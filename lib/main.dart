import 'package:flutter/material.dart';
import 'package:fluttersample/ui/SampleEntry.dart';

void main() => runApp(new FlutterApplication());

class FlutterApplication extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Welcome to Flutter',
      color: Colors.deepOrange,
      home: SampleEntry(),
    );
  }
}
