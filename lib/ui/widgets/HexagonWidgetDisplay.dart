import 'package:flutter/material.dart';
import 'package:fluttersample/ui/widgets/HexagonWidget.dart';

class HexagonWidgetDisplay extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _HexagonWidgetDisplayState();
  }
}

class _HexagonWidgetDisplayState extends State<HexagonWidgetDisplay> {
  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("六角星"),
      ),
      body: new Center(
        child: new HexagonWidget(),
      ),
    );

  }
}
