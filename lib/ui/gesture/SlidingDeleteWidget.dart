import 'package:flutter/material.dart';

class SlidingDeleteWidget extends StatefulWidget {
  final active;
  final ValueChanged<bool> onChanged;
  SlidingDeleteWidget({Key key, this.active: false, @required this.onChanged})
      : super(key: key);
  void _handleTap() {
    onChanged(!active);
  }

  @override
  State<StatefulWidget> createState() {
    return new _SlidingDeleteWidgetState();
  }
}

class _SlidingDeleteWidgetState extends State<SlidingDeleteWidget>
    with SingleTickerProviderStateMixin {
  double _left = 0.0;

  double _boundaryLeft = 100.0;
  double _boundaryRight = 0.0;
  // 中间分界线
  double _boundaryCenter = 50;

  Animation<double> animation;
  AnimationController controller;

  int _touchStartTimeStamp;
  int _touchLeaveTimeStamp;

  // 速度边界
  final int SpeedBoundary = 3;
  // 方向边界
  final double DirectionBoundary = 1.5;

  @override
  void initState() {
    super.initState();
  }

  void _toStartAnimation(double x) {
    if (controller == null) {
      controller = new AnimationController(
          duration: const Duration(milliseconds: 30), vsync: this);
    } else {
      // 动画重新执行的关键步骤，reset()是重置时间，让动画执行时间回复到了零时刻
      controller.reset();
    }
    // 定义 开始和结束动画数值
    animation = new Tween(begin: x, end: _boundaryRight).animate(controller)
      ..addListener(() {
        _left = animation.value;
        // print("-----_toStartAnimation:addListener:" + _left.toString());
        setState(() {});
      });
    controller.forward();
  }

  void _toEndAnimation(double x) {
    if (controller == null) {
      controller = new AnimationController(
          duration: const Duration(milliseconds: 30), vsync: this);
    } else {
      controller.reset();
    }
    animation = new Tween(begin: x, end: -_boundaryLeft).animate(controller)
      ..addListener(() {
        _left = animation.value;
        // print("-----_toEndAnimation:addListener:" + _left.toString());
        setState(() {});
      });
    controller.forward();
  }

  void toInitialAnimationState() {
    if (controller == null) {
      controller = new AnimationController(
          duration: const Duration(milliseconds: 30), vsync: this);
    } else {
      controller.reset();
    }
    animation = new Tween(begin: -_boundaryLeft, end: _boundaryRight)
        .animate(controller)
          ..addListener(() {
            _left = animation.value;
            // print("-----toInitialAnimationState:addListener:" + _left.toString());
            setState(() {});
          });
    controller.forward();
  }

  @override
  void dispose() {
    controller?.stop();
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // new Dismissible(),
    return new Padding(
      padding: EdgeInsets.only(bottom: 6),
      child: new Stack(
        children: <Widget>[
          new Container(
            height: 72,
            decoration: new BoxDecoration(
                //设置子控件背后的装饰
                color: Colors.red, //和Container下面的color会冲突 具体原因不详
                border: new Border.all(
                  //添加边框
                  width: 1.0, //边框宽度
                  color: Colors.red, //边框颜色
                ),
                borderRadius: new BorderRadius.all(Radius.circular(4.0)), //设置圆角
                boxShadow: <BoxShadow>[
                  //设置阴影
                  new BoxShadow(
                    color: Colors.grey[200], //阴影颜色
                    blurRadius: 4.0, //阴影大小
                  ),
                ]),
            child: new Row(
              children: <Widget>[
                new Expanded(
                  flex: 1,
                  child: new Container(),
                ),
                new Container(
                  width: _boundaryLeft,
                  height: 72,
                  color: Colors.transparent,
                  child: new Center(
                    child: new Text("删除"),
                  ),
                )
              ],
            ),
          ),
          Positioned(
            left: _left,
            child: GestureDetector(
              child: new Container(
                height: 72,
                width: MediaQuery.of(context).size.width,
                decoration: new BoxDecoration(
                  //设置子控件背后的装饰
                  color: Colors.white, //和Container下面的color会冲突 具体原因不详
                  border: new Border.all(
                    //添加边框
                    width: 1.0, //边框宽度
                    color: Colors.white, //边框颜色
                  ),
                  borderRadius:
                      new BorderRadius.all(Radius.circular(4.0)), //设置圆角
                ),
              ),
              //手指滑动时会触发此回调
              onPanUpdate: (DragUpdateDetails e) {
                if (_left >= -_boundaryLeft) {
                  _left += e.delta.dx;
                }
                if (_left < -_boundaryLeft) {
                  _left = -_boundaryLeft;
                }
                if (_left > 0) {
                  _left = _boundaryRight;
                }
                print(_left);
                setState(() {});
              },

              onPanStart: (DragStartDetails details) {
                print("-----onPanStart:");
                // 在触摸开始的时候，停止之前的动画
                _touchStartTimeStamp = DateTime.now().millisecondsSinceEpoch;
              },
              onPanEnd: (DragEndDetails details) {
                _touchLeaveTimeStamp = DateTime.now().millisecondsSinceEpoch;
                int diffTime = _touchLeaveTimeStamp - _touchStartTimeStamp;
                double diffDistance = details.velocity.pixelsPerSecond.distance;
                double threshold = diffDistance / diffTime;
                double direction = details.velocity.pixelsPerSecond.direction;
                print("-----onPanEnd:");
                print(
                    "-----onPanEnd:->direction:" + direction.abs().toString());
                print("-----onPanEnd:->delta:" + diffTime.toString());
                print("-----onPanEnd:->distance:" + diffDistance.toString());
                print("-----onPanEnd:->v:" + threshold.toString());
                print("-----onPanEnd:->_left:" + _left.toString());
                if (threshold > SpeedBoundary) {
                  print("-----onPanEnd:速度优先->:");
                  if (direction.abs() > DirectionBoundary) {
                    _toEndAnimation(_left);
                  } else {
                    _toStartAnimation(_left);
                  }
                } else {
                  print("-----onPanEnd:距离优先->:");
                  if (_left < -_boundaryCenter) {
                    _toEndAnimation(_left);
                  } else if (_left > -_boundaryCenter) {
                    _toStartAnimation(_left);
                  }
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
