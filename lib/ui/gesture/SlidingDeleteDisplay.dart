import 'package:flutter/material.dart';
import 'package:fluttersample/ui/gesture/SlidingDeleteWidget.dart';

class SlidingDeleteDisplay extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _SlidingDeleteDisplayState();
  }
}

class _SlidingDeleteDisplayState extends State<SlidingDeleteDisplay>
    with SingleTickerProviderStateMixin {
  List<String> strItems = <String>[
    '图标 -> keyboard',
    '图标 -> print',
    '图标 -> router',
    '图标 -> pages',
    '图标 -> zoom_out_map',
    '图标 -> zoom_out',
    '图标 -> youtube_searched_for',
    '图标 -> wifi_tethering',
    '图标 -> wifi_lock',
    '图标 -> widgets',
    '图标 -> weekend',
    '图标 -> web',
    '图标 -> accessible',
    '图标 -> ac_unit',
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
        
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("手势识别"),
      ),
      body: new ListView(
        children: strItems.map((f) {
          return new SlidingDeleteWidget();
        }).toList(),
      ),
    );
  }
}
