import 'package:flutter/material.dart';
import 'package:fluttersample/ui/left_scroll_actions/left_scroll_actions.dart';
import 'package:fluttersample/ui/left_scroll_actions/sample_list.dart';
import 'package:fluttersample/ui/left_scroll_actions/sample_list_scroll.dart';
import 'package:fluttersample/ui/left_scroll_actions/sample_row.dart';

class LeftScrollPage extends StatefulWidget {
  @override
  _LeftScrollPageState createState() => _LeftScrollPageState();
}

class _LeftScrollPageState extends State<LeftScrollPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Left Scroll Actions'),
        ),
        backgroundColor: Color(0xFFf5f5f4),
        body: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(12),
              child: MaterialButton(
                color: Colors.blue,
                child: Text(
                  'ListView Usage Demo',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ListPage(),
                  ));
                },
              ),
            ),
            Container(
              padding: EdgeInsets.all(12),
              child: MaterialButton(
                color: Colors.blue,
                child: Text(
                  'ClosableListView Usage Demo',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ClosableListPage(),
                  ));
                },
              ),
            ),
            Container(height: 50),
            Container(
              padding: EdgeInsets.only(top: 12, left: 8, bottom: 8),
              child: Text('These widget can scroll to actions.'),
            ),
            LeftScroll(
              buttonWidth: 80,
              child: Container(
                height: 60,
                color: Colors.white,
                alignment: Alignment.center,
                child: Text('👈 Try Scroll Left'),
              ),
              buttons: <Widget>[
                LeftScrollItem(
                  text: 'delete',
                  color: Colors.red,
                  onTap: () {
                    print('delete');
                  },
                ),
                LeftScrollItem(
                  text: 'Edit',
                  color: Colors.orange,
                  onTap: () {
                    print('edit');
                  },
                ),
              ],
              onTap: () {
                print('tap row');
              },
            ),
            LeftScroll(
              child: Container(
                height: 60,
                color: Colors.white.withOpacity(0.8),
                alignment: Alignment.center,
                child: Text('If opacity is not 1.0,may cause problem.'),
              ),
              buttons: <Widget>[
                LeftScrollItem(
                  text: 'delete',
                  color: Colors.red,
                ),
                LeftScrollItem(
                  text: 'Edit',
                  color: Colors.orange,
                ),
              ],
            ),
            Container(height: 50),
            Container(
              padding: EdgeInsets.only(top: 12, left: 8, bottom: 8),
              child:
                  Text('You can build widget like this if opacity is not 1.0.'),
            ),
            ExampleRow(
              onDelete: () {
                print('delete');
              },
              onTap: () {
                print('tap');
              },
              onEdit: () {
                print('edit');
              },
            ),
          ],
        ));
  }
}
