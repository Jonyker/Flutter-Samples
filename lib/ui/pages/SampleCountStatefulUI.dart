import 'package:flutter/material.dart';

class SampleCountStatefulUI extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _CounterState();
  }
}

class _CounterState extends State<SampleCountStatefulUI> {
  int _count = 0;
  void _increment() {
    setState(() {
      _count++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("StatefulWidget"),
      ),
      body: new Column(
        children: <Widget>[
          new RaisedButton(
            onPressed: _increment,
            child: new Text('Increment'),
          ),
          new Text('Count: $_count')
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        tooltip: 'add',
        child: new Icon(Icons.add),
        onPressed: null,
      ),
    );
  }
}
