import 'package:flutter/material.dart';
import 'package:fluttersample/utils/logutils.dart';

class Team extends StatefulWidget {
  @override
  _TeamPagesState createState() => _TeamPagesState();
}

class _TeamPagesState extends State<Team> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 255, 255, 255),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _peopleIcon(), // 联系人图标
          _textTeam(), // 团队文字
          _buildTextField(), // 搜索框
          _buttonRow(), // 点击框组
          new Expanded(
            child: TeamList(),
          )
        ],
      ),
    );
  }

  // 联系人图标按钮
  Widget _peopleIcon() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(360, 40, 0, 0),
      child: Container(
        height: 30,
        width: 30,
        decoration: new BoxDecoration(
          color: Color(0xFF9E9E9E), // 底色
          borderRadius: new BorderRadius.all(Radius.circular(5)), // 圆角
        ),
        child: IconButton(
          icon: Icon(Icons.person),
          color: Colors.white,
          onPressed: people,
          iconSize: 25,
          padding: const EdgeInsets.only(right: 1),
          highlightColor: Colors.cyan,
        ),
      ),
    );
  }

  // 联系人图标点击方法
  people() {
    print("点击了按钮");
  }

// 团队文字
  Widget _textTeam() {
    return Padding(
      padding: const EdgeInsets.only(right: 300),
      child: Text(
        "团队",
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.black54,
          fontSize: 35,
        ),
      ),
    );
  }

// 搜索框
  Widget _buildTextField() {
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 0),
      height: 45,
      child: Theme(
        data: new ThemeData(
            primaryColor: Colors.grey, hintColor: Colors.grey[200]),
        child: TextField(
          decoration: InputDecoration(
              fillColor: Colors.grey[200],
              filled: true,
              hintText: 'Search',
              prefixIcon: Icon(Icons.search),
              contentPadding: EdgeInsets.all(8.0),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
              )),
        ),
      ),
    );
  }

  // 点击框组
  Widget _buttonRow() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 15, 5, 0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: MyButton(
              text: "运营中心",
            ),
            flex: 1,
          ),
          Expanded(
            child: MyButton(
              text: "机构",
            ),
            flex: 1,
          ),
          Expanded(
            child: MyButton(
              text: "分支机构",
            ),
          ),
          Expanded(
            child: MyButton(
              text: "一级代理",
            ),
          ),
          Expanded(
            child: MyButton(
              text: "创客",
            ),
          ),
          Expanded(
            child: MyButton(
              text: "微创客",
            ),
          ),
        ],
      ),
    );
  }
}

// 自定义点击框
class MyButton extends StatelessWidget {
  final text;
  final double width;
  final double height;
  final textColor; // 字体颜色
  final backColor; // 背景颜色
  const MyButton(
      {this.text = '',
      this.width = 80,
      this.height = 40,
      this.textColor = Colors.black87,
      this.backColor = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black54, width: 1), //边框线
      ),
      child: RaisedButton(
        onPressed: () {
          print(this.text);
        },
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        color: this.backColor,
        highlightColor: Colors.black54,
        textColor: this.textColor,
        child: Text(
          this.text,
          softWrap: false,
        ),
      ),
    );
  }
}

// 团队列表
class TeamList extends StatefulWidget {
  _TeamListState createState() => _TeamListState();
}

class _TeamListState extends State<TeamList> {
  // List list = [
  //   {
  //     "name": "王一一（运营中心）",
  //     "phone": "15888888888",
  //     "zq：": "2018年5月",
  //     "jye": "45656895元"
  //   },
  //   {
  //     "name": "王二二（运营中心）",
  //     "phone": "15999999999",
  //     "周期：": "2018年5月",
  //     "5月交易额": "8456156元"
  //   }
  // ];

  List<Entity> list = [
    new Entity(
      "王二二（运营中心）",
      "15999999999",
      "2018年5月",
      "8456156元",
    ),
    new Entity(
      "王二二（运营中心）",
      "15999999999",
      "2018年5月",
      "8456156元",
    ),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: list.map((entity) {
          return new GestureDetector(
            child: _teamCard(entity),
          );
        }).toList(),
      ),
    );
  }

  Widget _teamCard(Entity entity) {
    print(entity.name);
    return Card(
      child: SizedBox(
        height: 150,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    entity.name,
                    textAlign: TextAlign.left,
                  ),
                  flex: 2,
                ),
                Expanded(
                  child: Text(
                    "周期：" +  entity.zq,
                    textAlign: TextAlign.right,
                  ),
                  flex: 1,
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  entity.phone,
                  textAlign: TextAlign.left,
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  "5月交易额" + entity.jye,
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Entity {
  Entity(this.name, this.phone, this.zq, this.jye);
  String name;
  String phone;
  String zq;
  String jye;
}
