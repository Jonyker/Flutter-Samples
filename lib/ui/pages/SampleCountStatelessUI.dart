import 'package:flutter/material.dart';

// 无状态的Widget
class SampleCountDisplay extends StatelessWidget {
  SampleCountDisplay({this.count});
  final int count;

  @override
  Widget build(BuildContext context) {
    return new Text('Count: $count');
  }
}

class SampleCountIncrementor extends StatelessWidget {
  SampleCountIncrementor({this.onPressed});
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      onPressed: onPressed,
      child: new Text('add'),
    );
  }
}

class SampleCountStatelessUI extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _CounterState();
  }
}

class _CounterState extends State<SampleCountStatelessUI> {

  int _count = 0;
  void _increment() {
    setState(() {
      _count++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("StatelessWidget"),
      ),
      body: new Column(
        children: <Widget>[
          new SampleCountIncrementor(onPressed: _increment),
          new SampleCountDisplay(count: _count),
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        tooltip: 'add',
        child: new Icon(Icons.add),
        onPressed: null,
      ),
    );
  }
}
