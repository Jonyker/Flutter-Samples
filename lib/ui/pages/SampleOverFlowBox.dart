import 'package:flutter/material.dart';

class SampleOverFlowBoxUI extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _SampleOverFlowBoxUIState();
  }
}

///
/// 设置了 minheight,minwidth ，子控件不会小于设置的范围；
/// 注意：
/// 设置 maxheight,和maxwidth的时候 它们的值不能小于父控件的宽高，如果有pading可以限定到 父控件的宽高减去pading的值。
/// 如： 当前例子中  注释1 ：父容器的宽高是200 减去pading后是180，那么 OverflowBox的值maxheight，maxwidth，就不能小于这个值180。否则会报错
///
class _SampleOverFlowBoxUIState extends State<SampleOverFlowBoxUI> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("OverflowBox"),
      ),
      body: new Container(
        //1 注意：父容器的宽高是200 减去pading后是180
        padding: EdgeInsets.all(10),
        color: Colors.green,
        width: 200,
        height: 200,
        child: new OverflowBox(
          maxHeight: 400, //2 不能小于父容器的高度180
          child: new Container(
            color: Colors.deepOrange,
            width: 200,
            height: 600,
          ),
        ),
      ),
    );
  }
}
