import 'package:flutter/material.dart';

class SampleUI extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _SampleUIState();
  }
}

class _SampleUIState extends State<SampleUI> {
  @override
  Widget build(BuildContext context) {
    // 标题区域
    Widget _titleSection = new Container(
      padding: const EdgeInsets.all(32.0),
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: new Text(
                    '标题行',
                    style: new TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                new Text(
                  '副标题行，副标题行，副标题行',
                  style: TextStyle(color: Colors.grey[500]),
                ),
              ],
            ),
          ),
          new Icon(
            Icons.star,
            color: Colors.red[500],
          ),
          new Text('41')
        ],
      ),
    );

    // 单个带有图片和文字的按钮
    Column buildButtonColumn(IconData icon, String label) {
      Color color = Theme.of(context).primaryColor;
      return new Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Icon(icon, color: color),
          new Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: new Text(
              label,
              style: TextStyle(
                  fontSize: 12.0, fontWeight: FontWeight.w400, color: color),
            ),
          ),
        ],
      );
    }

    Widget _buttonSection = new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          buildButtonColumn(Icons.call, "CALL"),
          buildButtonColumn(Icons.near_me, "ROUTE"),
          buildButtonColumn(Icons.share, "SHARE"),
        ],
      ),
    );

    Widget _textSection = new Container(
      padding: const EdgeInsets.all(32.0),
      child: new Text(
        '''
        Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese Alps. Situated 1,578 meters above sea level, it is one of the larger Alpine Lakes.
        ''',
        softWrap: true,
      ),
    );

    // 点评
    Widget _startSection = new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          new Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Icon(Icons.star, color: Colors.green[500]),
              new Icon(Icons.star, color: Colors.green[500]),
              new Icon(Icons.star, color: Colors.green[500]),
              new Icon(Icons.star, color: Colors.black),
              new Icon(Icons.star, color: Colors.black),
            ],
          ),
          new Text(
            "170 Reviews",
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w800,
                fontFamily: 'Roboto',
                letterSpacing: 0.5,
                fontSize: 20.0),
          )
        ],
      ),
    );

    var descTextStyle = new TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.w800,
        letterSpacing: 0.5,
        fontFamily: "Roboto",
        fontSize: 14.0,
        height: 2.0);
    // DefaultTextStyle.merge可以允许您创建一个默认的文本样式，该样式会被其
    var iconList = DefaultTextStyle.merge(
        style: descTextStyle,
        child: new Container(
          padding: new EdgeInsets.all(20.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new Column(
                children: <Widget>[
                  new Icon(Icons.kitchen, color: Colors.green[200]),
                  new Text('PREP'),
                  new Text('25 min')
                ],
              ),
              new Column(
                children: <Widget>[
                  new Icon(Icons.kitchen, color: Colors.green[200]),
                  new Text('COOK'),
                  new Text('20 min')
                ],
              ),
              new Column(
                children: <Widget>[
                  new Icon(Icons.kitchen, color: Colors.green[200]),
                  new Text('FEEDS'),
                  new Text('15 min')
                ],
              ),
            ],
          ),
        ));

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("布局"),
      ),
      body: new ListView(
        children: <Widget>[
          new Image.asset(
            'images/banner.jpeg',
            height: 240.0,
            fit: BoxFit.cover,
          ),
          _titleSection,
          _buttonSection,
          _textSection,
          _startSection,
          iconList
        ],
      ),
    );
  }
}
