import 'package:flutter/material.dart';

class UActionBar extends StatelessWidget {
  final title;

  UActionBar({this.title});

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: 50.0,
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      decoration: new BoxDecoration(color: Colors.red[500]),
      child: new Row(
        children: <Widget>[
          new IconButton(
            icon: new Icon(Icons.menu),
            tooltip: 'nav meau',
            onPressed: null,
          ),
          new Expanded(
            child: title,
          ),
          new IconButton(
            icon: new Icon(Icons.search),
            tooltip: 'search',
            onPressed: null,
          )
        ],
      ),
    );
  }
}

class SampleRowColumnUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          leading: new IconButton(
            icon: new InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: new Icon(Icons.arrow_back),
            ),
            tooltip: 'meau',
            onPressed: null,
          ),
          title: new Text('RowColumn'),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.search),
              tooltip: 'meau',
              onPressed: null,
            )
          ],
        ),
        body: new Column(
          children: <Widget>[
            new UActionBar(
              title: new Text('Hello World 12345',
                  style: Theme.of(context).primaryTextTheme.title),
            ),
            new Expanded(
              child: new Center(
                child: new Text('Hello World 12345'),
              ),
            )
          ],
        ));
  }
}
