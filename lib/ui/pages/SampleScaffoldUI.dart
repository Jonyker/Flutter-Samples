import 'package:flutter/material.dart';

class SampleScaffoldUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          leading: new IconButton(
            icon: new InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: new Icon(Icons.arrow_back),
            ),
            tooltip: 'meau',
            onPressed: null,
          ),
          title: new Text('Scaffold'),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.search),
              tooltip: 'meau',
              onPressed: null,
            )
          ],
        ),
        body: new Center(
          child: new Text('Hello World'),
        ),
        floatingActionButton: new FloatingActionButton(
          tooltip: 'add',
          child: new Icon(Icons.add),
          onPressed: null,
        ),);
  }
}
