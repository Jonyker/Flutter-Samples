import 'package:flutter/material.dart';
import 'package:fluttersample/ui/left_scroll_actions/samples.dart';
import 'package:fluttersample/ui/pages/InkResponsePage.dart';
import 'package:fluttersample/ui/pages/SampleKeyboardCover.dart';
import 'package:fluttersample/ui/pages/SampleOverFlowBox.dart';
import 'package:fluttersample/ui/pages/SampleRowColumnUI.dart';
import 'package:fluttersample/ui/pages/SampleScaffoldUI.dart';
import 'package:fluttersample/ui/pages/SampleCountStatefulUI.dart';
import 'package:fluttersample/ui/pages/SampleCountStatelessUI.dart';
import 'package:fluttersample/ui/pages/SampleShoppingUI.dart';
import 'package:fluttersample/ui/pages/SampleUI.dart';
import 'package:fluttersample/ui/pages/SampleNotifier.dart';
import 'package:fluttersample/arch/NormalArchMVP.dart';
import 'package:fluttersample/ui/pages/Team.dart';
import 'package:fluttersample/ui/widgets/HexagonWidgetDisplay.dart';
import 'package:fluttersample/ui/sketch/CustomPaintCanvasDisplay.dart';
import 'package:fluttersample/ui/gesture/SlidingDeleteDisplay.dart';

class SampleEntry extends StatelessWidget {

  List<Product> products  = [
                new Product(name: '水壶'),
                new Product(name: '高级烧水壶'),
                new Product(name: '大水壶'),
                new Product(name: '大茶壶'),
                new Product(name: '桌子上的大水壶'),
                new Product(name: '大水壶手柄'),
                new Product(name: '大水壶的壶盖'),
                new Product(name: '谁的水壶'),
                new Product(name: '我的壶'),
                new Product(name: '水壶里的水呢'),
                new Product(name: '大水壶多少钱'),
              ];
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Flutter Samples'),
      ),
      body: new ListView(
        children: <Widget>[
          _buildItem(context, "滑动删除", new LeftScrollPage()),
          _buildItem(context, "SampleScaffoldUI", new SampleScaffoldUI()),
          _buildItem(context, "SampleRowColumnUI", new SampleRowColumnUI()),
          _buildItem(context, "SampleCountStatefulUI", new SampleCountStatefulUI()),
          _buildItem(context, "SampleCountStatelessUI", new SampleCountStatelessUI()),
          _buildItem(context, "SampleUI", new SampleUI()),
          _buildItem(context, "NormalArchMVP", new NormalArchMVP()),
          _buildItem(context,"ShoppingList",new ShoppingList(products:products)),
          _buildItem(context, "HexagonWidgetDisplay", new HexagonWidgetDisplay()),
          _buildItem(context, "CustomPaintCanvasDisplay", new CustomPaintCanvasDisplay()),
          _buildItem(context, "SlidingDeleteDisplay", new SlidingDeleteDisplay()),
          _buildItem(context, "ValueNotifierCommunication", new ValueNotifierCommunication()),
          _buildItem(context, "KeyboardCover", new KeyboardCover()),
          _buildItem(context, "SlidingDeleteDisplay", new SlidingDeleteDisplay()),
          _buildItem(context, "Team", new Team()),
          _buildItem(context, "SampleOverFlowBoxUI", new SampleOverFlowBoxUI()),
          _buildItem(context, "InkResponsePageUI", new InkResponsePageUI()),
          
        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context, String title, Widget builder) {
    return new ListTile(
      leading: new Icon(Icons.star),
      title: new Text('$title'),
      onTap: () {
        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => builder),
        );
      },
    );
  }
}
