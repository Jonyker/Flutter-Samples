import 'dart:ui' as ui;
import 'package:flutter/material.dart';

class CustomPaintCanvasDisplay extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _CustomPaintCanvasDisplayState();
  }
}

class _CustomPaintCanvasDisplayState extends State<CustomPaintCanvasDisplay> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("自定义视图基础"),
      ),
      body: new Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: new CustomPaint(
          painter: new LinerPainter(),
        ),
      ),
    );
  }
}

///新建类继承于CustomPainter并且实现CustomPainter里面的paint（）和shouldRepaint方法
class LinerPainter extends CustomPainter {
  // Paint _paint = Paint()
  //   ..color = Colors.blueAccent //画笔颜色
  //   ..strokeCap = StrokeCap.round //画笔笔触类型
  //   ..isAntiAlias = true //是否启动抗锯齿
  //   ..blendMode = BlendMode.exclusion //颜色混合模式
  //   ..style = PaintingStyle.fill //绘画风格，默认为填充
  //   ..colorFilter = ColorFilter.mode(Colors.blueAccent,
  //       BlendMode.exclusion) //颜色渲染模式，一般是矩阵效果来改变的,但是flutter中只能使用颜色混合模式
  //   ..maskFilter = MaskFilter.blur(BlurStyle.inner, 3.0) //模糊遮罩效果，flutter中只有这个
  //   ..filterQuality = FilterQuality.high //颜色渲染模式的质量
  //   ..strokeWidth = 15.0; //画笔的宽度

  Paint _paint = new Paint()
    ..color = Colors.blueAccent
    ..strokeCap = StrokeCap.round
    ..isAntiAlias = true
    ..strokeWidth = 5.0
    ..style = PaintingStyle.stroke;
  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    // 绘制直线
    // canvas.drawLine(Offset(20.0, 20.0), Offset(100.0, 100.0), _paint);
    // canvas.drawLine(Offset(130.0, 80.0), Offset(220.0, 100.0), _paint);

    // 绘制点
    // canvas.drawPoints(
    //     ///PointMode的枚举类型有三个，points（点），lines（线，隔点连接），polygon（线，相邻连接）
    //     ui.PointMode.lines,
    //     [
    //       Offset(20.0, 130.0),
    //       Offset(100.0, 210.0),
    //       Offset(100.0, 310.0),
    //       Offset(200.0, 310.0),
    //       Offset(200.0, 210.0),
    //       Offset(280.0, 130.0),
    //       Offset(20.0, 130.0),
    //     ],
    //     _paint..color = Colors.redAccent);

    //绘制圆 参数(圆心，半径，画笔)
    canvas.drawCircle(
        Offset(200.0, 250.0),
        50.0,
        _paint
          ..color = Colors.deepPurple
          ..style = PaintingStyle.fill //绘画风格改为stroke
        );
  }

  ///
  /// 控制自定义View是否需要重绘的，返回false代表这个View在构建完成后不需要重绘。
  ///
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
