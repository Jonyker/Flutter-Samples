import 'package:flutter/services.dart';
import 'package:flutter/material.dart';

class  StatusUtils {
  static SystemUiOverlayStyle uiStyle = const SystemUiOverlayStyle(
        systemNavigationBarColor: const Color(0x00000000),
        systemNavigationBarDividerColor: null,
        statusBarColor: const Color(0x00000000),
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        statusBarBrightness: Brightness.dark,
    );

    static appUIStyle() {
        SystemChrome.setSystemUIOverlayStyle(uiStyle);
    }
}