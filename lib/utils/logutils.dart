
class LogUtils {

  static bool isDebug = true;
  static void logTAG(String tag, String log) {
    if (isDebug) {
      print("---" + tag + ":" + log);
    }
  }

  static void logOBJTAG(String tag,Object log) {
    if (isDebug) {
      print("--------------start:"+tag+"--------------↓↓↓");
      print(log);
      print("----------------end:"+tag+"--------------↑↑↑");
    }
  }

}
