import 'package:flutter/material.dart';
import 'package:fluttersample/arch/mvp.dart';

class NormalArchMVP extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    print("---NormalArchMVP:createState");
    _NormalArchMVPState _vState = new _NormalArchMVPState();
    ContactPresenter _presenter = new ContactPresenter(_vState);
    _presenter.init();
    return _vState;
  }
}

class _NormalArchMVPState extends State<NormalArchMVP> implements View {
  ContactPresenter _presenter;
  List<Contact> contacts = [];

  @override
  setPresenter(Presenter presenter) {
    _presenter = presenter;
  }

  @override
  void initState() {
    super.initState();
    _presenter.loadContacts();
  }
 
  Widget buildListTile(BuildContext context, Contact contact) {
    return new MergeSemantics(
      child: new ListTile(
        isThreeLine: true,
        dense: false,
        leading: new ExcludeSemantics(
          child: new CircleAvatar(
            child: new Text(contact.fullName.substring(0, 1)),
          ),
        ),
        title: new Text(contact.fullName),
        subtitle: new Text(contact.email),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget widget;
    widget = new ListView.builder(
      padding: new EdgeInsets.symmetric(vertical: 8.0),
      itemBuilder: (BuildContext context, int index) {
        return buildListTile(context, contacts[index]);
      },
      itemCount: contacts.length,
    );

    return new Scaffold(
        appBar: new AppBar(
          title: new Text("MVP"),
        ),
        body: widget);
  }

  @override
  void onLoadContactsComplete(List<Contact> items) {
    setState(() {
      contacts = items;
      print("---NormalArchMVP:onLoadContactsComplete->contacts.size:${contacts.length}");
    });
  }

  @override
  void onLoadContactsError() {
    print("---NormalArchMVP:onLoadContactsError");
  }
}
